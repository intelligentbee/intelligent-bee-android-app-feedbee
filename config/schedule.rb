every :sunday, :at => '11pm' do # Use any day of the week or :weekend, :weekday
  runner "User.update_all(:voted => false)"
  rake "db:migrate"
end