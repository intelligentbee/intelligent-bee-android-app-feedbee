Apibee::Application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :votes

      resources :users do
        collection do
          post 'find_user_by_email'
          post 'update_user'
          post 'voted_status'
        end
      end

      resources :sessions

      resources :products do
        collection do
          post 'vote'
        end
      end

    end
  end
  get "apis/new_user"
      
  resources :apis  
  
  match '/create_user',     to: "apis#create_user",     via: 'post'
  match '/create_session',  to: "apis#create_session",  via: 'post'
  match '/current_user',    to: "apis#current_user",    via: 'post'
  match '/vote_up',         to: "apis#vote_up",         via: 'post'
  match '/vote_down',       to: "apis#vote_down",       via: 'post'
  match '/user_index',      to: "apis#user_index" ,     via: 'get'
  match '/user_index',       to: "apis#user_index" ,     via: 'post'
  match '/apis/update_votes',to: "apis#update_votes",   via: 'get'
  match '/apis/update_votes',to: "apis#update_votes",   via: 'post'
  match '/apis/delete_user', to: "apis#delete_user",    via: 'post'
end
