module Api
  module V1

    class UsersController < ApplicationController

      def find_user_by_email
        user = User.find_by(email: params[:email])
        if user
          message = { "status" => "success", "message" => user.remember_token }
          respond_to do |f|
            f.json { render json: message } 
          end
          else
          message = { "status" => "error", "message" => "User not found" }
          respond_to do |f|
            f.json { render json: message }
          end
        end
      end

      def update_user
        user = User.find_by(remember_token: params[:token])
        user.update_attributes(password: params[:password], password_confirmation: params[:password_confirmation])
        if user.save
          message = {"status" => "success", "message" => "Password successfully updated"}
          render json: message
        else
          message = {"status" => "error", "message" => "Password was not successfully updated"}
          render json: message
        end
      end

      def voted_status
        user = User.find_by(email: params[:email])
        if user
          render json: user.voted
        else
          message = {"status" => "error", "message" => "Please come again."}
          render json: message
        end
      end

    end
  end
end