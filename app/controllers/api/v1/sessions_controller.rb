module Api
  module V1

    class SessionsController < ApplicationController
	  
	  # Commented lines reason: this function is also used when 
	  # logging in from android. The android app doesn't have, in the first version
	  # the possibility to refresh the token.
	  def create
        @user = User.find_by(email: params[:email].downcase)
        if @user && @user.authenticate(params[:password])
         # remember_token = User.new_remember_token
         # @user.update_attribute(:remember_token, User.encrypt(remember_token))
          render json: @user
        else
          message = {"status" => "error", "is_server_error" => "true", "message" => "Something went wrong."}
          render json: message
        end
      end

    end
  end
end