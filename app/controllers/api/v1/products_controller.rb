module Api
  module V1

    class ProductsController < ApplicationController

	 # renders all the products in the database ordered by number of votes
	 # Pre-condition: the request should have in the header the user token
	 # WARNING: BE ABSOLUTELY SURE THAT WHEN YOU MOVE LIVE THIS VERSION OF 
	 # 			THAT THE BROWSER BASED APP ADDS IN THE HEADER OF THE REQUEST 
     #			THE USER TOKEN
	 # WARNING: BE SURE THAT THE BROWSER BASED APP RENDERS IN IT'S TABLE 
	 # 			THE PRODUCTS FROM products_list. SUCCESS MESSAGE STRUCTURE HAS CHANGED
     def index
		product = Product.order("votes desc").load
		user    = User.find_by(remember_token: request.headers[:token])
		if product && user
			message = {"status"=>"success", "message"=> "We have products!", "products_list" => product.as_json, "user_voted" => user.voted}
			respond_to do |f|
				f.json {render json: message}
				f.xml {render xml: message }
			end
		end
	 end
    
      def new
        product = Product.new
        respond_to do |f|
          f.json {render json: product}
        end 
      end
    
	  # removed checking the user.voted when creating a new product. 
	  # reason: when a user votes down a product a create product request (from android) is invoked as well
	  # the user should be allowed to create a new product even if the vote down request has been completed ( user.voted=true )
      def create
	  @product = Product.where(name: params[:name]).take
	  if @product
  	    message = {"status" => "error", "is_server_error" => "true", "message" => "Product already exists! Could not create."}
 	    render json: message
	  else
		product = Product.new(name: params[:name])
		user    = User.find_by(remember_token: params[:token])
		vote    = Vote.new(user_name: user.name, action: "up", product_name: product.name)

	    product.increment(:votes, 1)
	    if vote.save && product.save && user.save
	    message = {"status" => "success", "message" => "Product created.", "product" => product.as_json }
 			render json: message
	    else
	      message = {"status" => "error", "is_server_error" => "true", "message" => "Could not create product."}
 	      render json: message
	    end
	  end
	end
	  
	  # product_id 		 - the product that is being voted on, either up or down
	  # adapter_position - a number that is being sent from the android app, it just means the index of the view that the user clicked on
	  # 				   this should just be returned in all the success messages, 
	  # 				   you shouldn't do anything with it, but it's very important that you return it 
	  # 				   in all the success messages.
	  # is_server_error - says to the android app or the browser app ( if you'll implement something ) that the error message being returned is from the api
	  # 				  application. Reason: the android app uses the same json format of the error message when 
	  #					  confronting with network errors ( user web access is off,
	  # 				  server is not reachable, etc ) in those messages, is_server_error = false
      def vote
        product = Product.find(params[:product_id])
        user    = User.find_by(remember_token: params[:token])
        user_action  = params[:user_action]

        if user.voted == false
          if user_action == "up"
            vote = Vote.new(user_name: user.name, action: "up", product_name: product.name)
            user.update_attribute(:voted, true)
            product.increment(:votes, 1)
            if vote.save && product.save && user.save
              message = {"status" => "success", "message" => "Great success.", "product_id" => params[:product_id], "adapter_position" => params[:adapter_position], "user_voted" => user.voted}
                render json: message
            else
              message = {"status" => "error", "is_server_error" => "true", "message" => "Could not vote up."}
              render json: message
            end
            
          elsif user_action == "down"
            vote = Vote.new(user_name: user.name, action: "down", product_name: product.name)
            user.update_attribute(:voted, true)
            product.decrement(:votes, 1)
            if vote.save &&  product.save && user.save
              message = {"status" => "success", "message" => "Great success.", "product_id" => params[:product_id], "adapter_position" => params[:adapter_position], "user_voted" => user.voted}
              respond_to do |f|
                f.json {render json: message}
              end
            else
              message = {"status" => "error", "is_server_error" => "true", "message" => "Could not vote down."}
              render json: message
            end
          
          else
            message = {"status" => "error", "is_server_error" => "true", "message" => "Action - #{user_action} - must by either up or down."}
            render json: message
          end

        else
          message = {"status" => "error", "is_server_error" => "true", "message" => "User has already voted this week."}
          render json: message
        end

      end

    end
  end
end
