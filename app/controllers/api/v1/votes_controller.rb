module Api
  module V1

    class VotesController < ApplicationController

      def index
        vote = Vote.all
        respond_to do |f|
          f.json { render json: vote }
        end
      end

    end
  end
end