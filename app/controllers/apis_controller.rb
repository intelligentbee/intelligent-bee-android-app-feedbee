require 'json'

class ApisController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  #grabs all Products from the Product model
	def index
		product = Product.order("votes desc").load
		user    = User.find_by(remember_token: request.headers[:token])
		if product && user
			message = {"status"=>"success", "message"=> "We have products!", "products_list" => product.as_json, "user_voted" => user.voted}
			respond_to do |f|
				f.json {render json: message}
				f.xml {render xml: message }
			end
		end
	end

	#creates a new Product
	def create
	  @product = Product.where(name: params[:name]).take
	  if @product
  	    message = {"status" => "error", "is_server_error" => "true", "message" => "Product already exists! Could not create."}
 	    render json: message
	  else
		product = Product.new(name: params[:name])
		user    = User.find_by(remember_token: params[:token])
		vote    = Vote.new(user_name: user.name, action: "up", product_name: product.name)

	    product.increment(:votes, 1)
	    if vote.save && product.save && user.save
	    message = {"status" => "success", "message" => "Product created.", "product" => product.as_json }
 			render json: message
	    else
	      message = {"status" => "error", "is_server_error" => "true", "message" => "Could not create product."}
 	      render json: message
	    end
	  end
	end
	
	def vote_up
    product = Product.find(params[:product_id])
    user    = User.find_by(remember_token: params[:token])
	if user.voted == false
		
		vote    = Vote.new(user_name: user.name, action: "up", product_name: product.name)

		user.update_attribute(:voted, true)
		product.increment(:votes, 1)

		if vote.save && product.save && user.save
		  message = {"status" => "success", "message" => "Great success.", "product_id" => params[:product_id], "adapter_position" => params[:adapter_position], "user_voted" => user.voted}
			respond_to do |f|
			  f.json {render json: message}
			end
		  else
			message = {"status" => "error", "is_server_error" => "true", "message" => "Could not vote up."}
			render json: message
		  end
	else 
		message = {"status" => "error", "is_server_error" => "true", "message" => "Could not vote.", "user_voted" => user.voted}
		render json: message
	end
  end
  
  def vote_down
    product = Product.find(params[:product_id])
    user    = User.find_by(remember_token: params[:token])
	if user.voted == false
		vote    = Vote.new(user_name: user.name, action: "down", product_name: product.name)

		user.update_attribute(:voted, true)
		product.decrement(:votes, 1)
		if vote.save &&  product.save && user.save

		  message = {"status" => "success", "message" => "Great success.",  "product_id" => params[:product_id], "adapter_position" => params[:adapter_position], "user_voted" => user.voted}
			respond_to do |f|
			  f.json {render json: message}
			end
		  else
			message = {"status" => "error", "is_server_error" => "true", "message" => "Could not vote down."}
			render json: message
		  end
	else
		message = {"status" => "error", "is_server_error" => "true", "message" => "Could not vote."}
		render json: message
	end
  end


  def show_users
  	user = User.find(params[:id])
  		respond_to do |f|
  			f.json {render json: user}
  	end
	end

  def new_user
 	  user = User.new
 	  respond_to do |f|
 	 	  f.json {render json: user}
 	  end
  end

  def create_user
 	  @user = User.new(name: params[:name], email: params[:email], 
 	          password: params[:password], password_confirmation: params[:password_confirmation])
 	
 	  if @user.save
 	      message = {"status" => "success", "message" => "User created.", "token" => @user.remember_token, "email" => @user.email }
     	  respond_to do |f|
			f.json {render json: message}
 	    end
 	  else
 		  message = {"status" => "error", "is_server_error" => "true", "message" => "Could not create user.", "errors" => @user.errors.as_json}
		  respond_to do |f|
 	 	    f.json {render json: message}
 	    end
 	  end
  end
  
  def create_session
    @user = User.find_by(email: params[:email].downcase)
    if @user && @user.authenticate(params[:password])
  	  respond_to do |f|
  	    f.json  { render json: @user}
  	  end
  	else
  	  message = {"status" => "error", "is_server_error" => "true", "message" => "Invalid email/password combination"}
	  render json: message
  	end
  end

  def user_index
  	allusers = User.load
  		  respond_to do |f|
  			 f.json {render json: allusers}
        end
  end

  #update a particular collumn
  def update_votes
    User.update_all("voted = false")
    message = {"status" => "success", "message" => "Updated"}
     respond_to do |f|
      f.json {render json: message["message"]}
     end
  end

  def delete_user
    @user = User.find(params[:id])
    #delete the user
    User.destroy(@user)
    message = {"status" => "success", "message" => "Deleted"}
      respond_to do |f|
        f.json { render json: message["message"] }
      end
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :voted)
    end
  
end

