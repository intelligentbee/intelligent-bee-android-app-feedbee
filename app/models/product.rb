class Product < ActiveRecord::Base
  after_initialize :default_values
  before_save { self.name = name.downcase }
  
  attr_accessible :name, :votes
  validates :name, presence: true, length: { maximum: 50},
                    uniqueness: { case_sensitive: false}
  validates :votes, presence: true
  
  private
    def default_values
      self.votes ||= "0"
    end
end
