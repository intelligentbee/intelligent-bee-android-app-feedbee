class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.string :user_name
      t.string :action
      t.string :product_name

      t.timestamps
    end
    add_index :votes, [:created_at]
  end
end
